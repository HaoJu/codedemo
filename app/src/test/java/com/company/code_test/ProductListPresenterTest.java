package com.company.code_test;

import com.company.code_test.model.Rate;
import com.company.code_test.model.Transaction;
import com.company.code_test.presenter.ProductListPresenter;
import com.company.code_test.util.TestResourcesLoader;
import com.company.code_test.utility.DomainDataReader;
import com.company.code_test.utility.TransactionValueExchangeConverter;
import com.company.code_test.view.ProductListView;
import com.company.code_test.view.model.TransactionViewModel;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;

/**
 * ProductListPresenterTest.
 */

@RunWith(MockitoJUnitRunner.class)
public class ProductListPresenterTest {

    @Mock
    ProductListView mView;
    @Mock
    DomainDataReader mReader;

    private Gson mGson;
    private ProductListPresenter mPresenter;

    @Before
    public void setUp() {
        mGson = new GsonBuilder().setFieldNamingPolicy(
                FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();
        mPresenter = new ProductListPresenter(mView, mReader);
    }

    @Test
    public void shouldConvertTransactionToTransactionViewModels() {
        List<Transaction> transactions = readTransactions();
        TransactionViewModel transaction1 = mPresenter.convertToViewModels(transactions.get(0));
        TransactionViewModel transaction2 = mPresenter.convertToViewModels(transactions.get(1));
        assertThat(transaction1, is(notNullValue()));
        assertThat(transaction2, is(notNullValue()));
        assertThat(transaction1.mTransactionAmount, is("1"));
        assertThat(transaction1.mProductID, is("J4064"));
        assertThat(transaction1.mTransactionCurrency, is("GBP"));
    }

    @Test
    public void shouldGroupTransactionsByProductId() {
        List<TransactionViewModel> transactionViewModels = convertToViewModels();
        ArrayList<ArrayList<TransactionViewModel>> groupedTransactions = new ArrayList<>();
        mPresenter.groupTransactionsByProductID(transactionViewModels, groupedTransactions);
        assertThat(groupedTransactions, hasSize(1));
        assertThat(groupedTransactions.get(0), hasSize(4)); //J4064
    }

    @Test
    public void shouldExchangeNonGBPValueToGBPValue() {
        List<TransactionViewModel> transactions = convertToViewModels();
        List<Rate> rates = readExchangeRates();
        TransactionValueExchangeConverter exchangeConverter = new TransactionValueExchangeConverter();
        List<TransactionViewModel> convertedTransactions = mPresenter.applyExchange(transactions, exchangeConverter, rates);
        assertEquals(convertedTransactions.get(0).mMonetaryValueInGBP, "£1.00");
        assertEquals(convertedTransactions.get(0).mMonetaryValueInOwnCurrency, "£1.00");
        assertEquals(convertedTransactions.get(1).mMonetaryValueInGBP, "£0.50");
        assertEquals(convertedTransactions.get(1).mMonetaryValueInOwnCurrency, "€1.00");
        assertEquals(convertedTransactions.get(2).mMonetaryValueInGBP, "£0.71");
        assertEquals(convertedTransactions.get(2).mMonetaryValueInOwnCurrency, "$1.00");
        assertEquals(convertedTransactions.get(3).mMonetaryValueInGBP, "£0.71");
        assertEquals(convertedTransactions.get(3).mMonetaryValueInOwnCurrency, "A$1.00");
    }

    private List<TransactionViewModel> convertToViewModels() {
        List<Transaction> transactions = readTransactions();
        List<TransactionViewModel> transactionViewModels = new ArrayList<>();
        for (Transaction transaction : transactions) {
            TransactionViewModel vm = mPresenter.convertToViewModels(transaction);
            transactionViewModels.add(vm);
        }
        return transactionViewModels;
    }

    private List<Transaction> readTransactions() {
        return mGson.fromJson(TestResourcesLoader.loadJSONFromAsset("responses/GET/second_data_set/transactions.json"),
                new TypeToken<List<Transaction>>() {
                }.getType());
    }

    private List<Rate> readExchangeRates() {
        return mGson.fromJson(TestResourcesLoader.loadJSONFromAsset("responses/GET/second_data_set/rates.json"),
                new TypeToken<List<Rate>>() {
                }.getType());
    }

}
