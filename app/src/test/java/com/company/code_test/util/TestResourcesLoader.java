package com.company.code_test.util;

import java.io.IOException;
import java.io.InputStream;

/**
 * TestResourcesLoader.
 */
public class TestResourcesLoader {

    public static String loadJSONFromAsset(String filename) {
        String json;
        try {
            InputStream is = ClassLoader.getSystemResourceAsStream(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
