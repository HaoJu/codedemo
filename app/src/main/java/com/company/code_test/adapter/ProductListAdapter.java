package com.company.code_test.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.company.code_test.R;
import com.company.code_test.view.holder.ProductCellViewHolder;
import com.company.code_test.view.model.TransactionViewModel;

import java.util.ArrayList;

/**
 * ProductListAdapter for ProductListActiviy.
 */
public class ProductListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "ProductListAdapter";
    private ArrayList<ArrayList<TransactionViewModel>> mProductList;
    private CellClickListener mCellClickListener;

    public ProductListAdapter(ArrayList<ArrayList<TransactionViewModel>> productList, CellClickListener cellClickListener) {
        mProductList = productList;
        mCellClickListener = cellClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_list_cell, parent, false);
        ProductCellViewHolder viewHolder = new ProductCellViewHolder(view, mCellClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ProductCellViewHolder viewHolder = (ProductCellViewHolder) holder;
        ArrayList<TransactionViewModel> entry = mProductList.get(position);
        viewHolder.bindProductEntry(entry);
    }

    @Override
    public int getItemCount() {
        return mProductList.size();
    }

    public interface CellClickListener {
        void onCellClicked(String productID, ArrayList<TransactionViewModel> transactions);
    }
}
