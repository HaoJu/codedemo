package com.company.code_test.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.company.code_test.R;
import com.company.code_test.view.holder.TransactionCellViewHolder;
import com.company.code_test.view.model.TransactionViewModel;

import java.util.ArrayList;

/**
 * TransactionListAdapter for TransactionListActivity.
 */
public class TransactionListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "TransactionListAdapter";
    private ArrayList<TransactionViewModel> mTransactions;

    public TransactionListAdapter(ArrayList<TransactionViewModel> transactions) {
        mTransactions = transactions;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transaction_list_cell, parent, false);
        TransactionCellViewHolder viewHolder = new TransactionCellViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TransactionCellViewHolder viewHolder = (TransactionCellViewHolder) holder;
        viewHolder.bindTransaction(mTransactions.get(position));
    }

    @Override
    public int getItemCount() {
        return mTransactions.size();
    }
}
