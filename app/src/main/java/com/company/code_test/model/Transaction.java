package com.company.code_test.model;

/**
 * Transaction domain model.
 */
public class Transaction {
    public String amount;
    public String sku;
    public String currency;
}
