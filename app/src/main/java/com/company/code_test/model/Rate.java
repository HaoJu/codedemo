package com.company.code_test.model;

/**
 * Rate domain model.
 */
public class Rate {
    public String from;
    public String rate;
    public String to;
}
