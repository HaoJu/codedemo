package com.company.code_test.utility;

import com.company.code_test.model.Rate;
import com.company.code_test.view.model.TransactionViewModel;

import java.math.BigDecimal;
import java.util.List;

/**
 * TransactionValueExchangeConverter, convert and format transaction values.
 */
public class TransactionValueExchangeConverter {

    private static final String GBP = "GBP";
    private static final String USD = "USD";
    private static final String CAD = "CAD";
    private static final String AUD = "AUD";
    private static final String EUR = "EUR";
    private static final String BASE_EXCHANGE_RATE = "1.00";
    private static final String CAD_TO_GBP_EXCHANGE_RATE = "0.7084"; // CAD to GBP is not provided in the feed.

    public MonetaryValueHolder applyExchange(TransactionViewModel transaction, List<Rate> exchangeRates) {
        Rate exchangeRate = findGBPExchangeRate(transaction.mTransactionCurrency, exchangeRates);
        BigDecimal valueInOwnCurrency = new BigDecimal(transaction.mTransactionAmount);
        BigDecimal valueInGBP = exchange(valueInOwnCurrency, exchangeRate);
        return formatMonetaryValue(transaction.mTransactionCurrency, valueInOwnCurrency, valueInGBP);
    }

    /**
     * Convert non GBP value into GBP value based on the exchange rate giving.
     * @param valueInOwnCurrency
     * @param exchangeRate
     * @return
     */
    private BigDecimal exchange(BigDecimal valueInOwnCurrency, Rate exchangeRate) {
        BigDecimal rate = new BigDecimal(exchangeRate.rate);
        BigDecimal valueInGBP = valueInOwnCurrency.multiply(rate);
        valueInOwnCurrency = valueInOwnCurrency.setScale(2, BigDecimal.ROUND_HALF_UP);
        valueInGBP = valueInGBP.setScale(2, BigDecimal.ROUND_HALF_UP);
        return valueInGBP;
    }

    /**
     * Find matching exchange rate, e.g.if transaction currency is USD, then the desired exchange rate should be USD to GBP.
     * @param transactionCurrency
     * @param exchangeRates
     * @return
     */
    private Rate findGBPExchangeRate(String transactionCurrency, List<Rate> exchangeRates) {
        // base exchange rate.
        Rate baseExchangeRate = new Rate();
        baseExchangeRate.from = transactionCurrency;
        baseExchangeRate.to = GBP;
        baseExchangeRate.rate = BASE_EXCHANGE_RATE;

        // CAD to GBP rate.
        Rate exchangeRate = new Rate();
        exchangeRate.from = CAD;
        exchangeRate.to = GBP;
        exchangeRate.rate = CAD_TO_GBP_EXCHANGE_RATE;

        // find the desired rate.
        for (Rate rate : exchangeRates) {
            if (rate.from.equalsIgnoreCase(transactionCurrency) && rate.to.equalsIgnoreCase(GBP)) {
                exchangeRate = rate;
                break;
            }
            if (transactionCurrency.equalsIgnoreCase(GBP)) {
                exchangeRate = baseExchangeRate;
                break;
            }
        }
        return exchangeRate;
    }

    /**
     * Format BigDecimal value to Monetary display value, e.g. £45.98, CA$34.33.
     * @param ownCurrency
     * @param valueInOwnCurrency
     * @param valueInGBP
     * @return
     */
    private MonetaryValueHolder formatMonetaryValue(String ownCurrency, BigDecimal valueInOwnCurrency, BigDecimal valueInGBP) {
        String currencyInDisplayFormat = "";
        switch (ownCurrency) {
            case GBP:
                currencyInDisplayFormat = "£";
                break;
            case USD:
                currencyInDisplayFormat = "$";
                break;
            case CAD:
                currencyInDisplayFormat = "CA$";
                break;
            case AUD:
                currencyInDisplayFormat = "A$";
                break;
            case EUR:
                currencyInDisplayFormat = "€";
                break;
            default:
                currencyInDisplayFormat = "£";
                break;
        }

        String valueInOwnCurrencyFormatted = currencyInDisplayFormat + String.format("%.2f", valueInOwnCurrency.doubleValue());
        String valueInGBPFormatted = "£" + String.format("%.2f", valueInGBP.doubleValue());
        MonetaryValueHolder monetaryValueHolder = new MonetaryValueHolder();
        monetaryValueHolder.mMonetaryValueInOwnCurrency = valueInOwnCurrencyFormatted;
        monetaryValueHolder.mMonetaryValueInGBP = valueInGBPFormatted;
        monetaryValueHolder.mValueInGBP = valueInGBP;
        return monetaryValueHolder;
    }

    public class MonetaryValueHolder {
        public String mMonetaryValueInOwnCurrency;
        public String mMonetaryValueInGBP;
        public BigDecimal mValueInGBP;
    }
}
