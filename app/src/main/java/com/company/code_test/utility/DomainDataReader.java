package com.company.code_test.utility;

import android.content.Context;

import com.company.code_test.model.Rate;
import com.company.code_test.model.Transaction;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.List;

/**
 * DomainDataReader, read transactions and rates from IO media.
 */
public class DomainDataReader {

    private Context mContext;
    private Gson mGson;

    public DomainDataReader(Context context, Gson gson) {
        mContext = context;
        mGson = gson;
    }

    public List<Transaction> readTransactions(final String fileName) {
        Type listType = new TypeToken<List<Transaction>>() { }.getType();
        return mGson.fromJson(loadJSONFromResources(fileName), listType);
    }

    public List<Rate> readExchangeRates(final String fileName) {
        Type listType = new TypeToken<List<Rate>>() { }.getType();
        return mGson.fromJson(loadJSONFromResources(fileName), listType);
    }

    /**
     * Loads Json text file from Assets.
     * @param filename
     * @return
     */
    private String loadJSONFromResources(String filename) {
        BufferedReader reader = null;
        String json = null;
        StringBuilder sb = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(mContext.getAssets().open(filename)));
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            json = sb.toString();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return json;
    }
}
