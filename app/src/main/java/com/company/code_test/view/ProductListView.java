package com.company.code_test.view;

/**
 * ProductListView.
 */
public interface ProductListView {

    void notifyUIForDataSetChange();

    void reportError(String message);

}
