package com.company.code_test.view.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.company.code_test.R;
import com.company.code_test.adapter.ProductListAdapter;
import com.company.code_test.view.model.TransactionViewModel;

import java.util.ArrayList;

/**
 * ProductCellViewHolder.
 */
public class ProductCellViewHolder extends RecyclerView.ViewHolder {

    private final View mView;
    private final TextView mProductIDTextView;
    private final TextView mTransactionNumberTextView;
    private ProductListAdapter.CellClickListener mCellClickListener;

    public ProductCellViewHolder(View itemView, final ProductListAdapter.CellClickListener cellClickListener) {
        super(itemView);
        mView = itemView;
        mCellClickListener = cellClickListener;
        mProductIDTextView = (TextView) mView.findViewById(R.id.product_id);
        mTransactionNumberTextView = (TextView) mView.findViewById(R.id.transaction_number);
    }

    public void bindProductEntry(final ArrayList<TransactionViewModel> entry) {
        final String productID = entry.get(0).mProductID;
        mProductIDTextView.setText(productID);
        mTransactionNumberTextView.setText(String.valueOf(entry.size()) + " " + mView.getResources().getString(R.string.transactions));
        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCellClickListener.onCellClicked(productID, entry);
            }
        });
    }
}
