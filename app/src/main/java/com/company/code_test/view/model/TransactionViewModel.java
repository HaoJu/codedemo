package com.company.code_test.view.model;

import org.parceler.Parcel;

import java.math.BigDecimal;

/**
 * TransactionViewModel.
 */

@Parcel
public class TransactionViewModel {
    public String mTransactionAmount = "";
    public String mProductID = "";
    public String mTransactionCurrency = "";
    public String mMonetaryValueInOwnCurrency = "";
    public String mMonetaryValueInGBP = "";
    public BigDecimal mTransactionAmountInGBP = new BigDecimal(0);
}
