package com.company.code_test.view.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.company.code_test.R;
import com.company.code_test.view.model.TransactionViewModel;

/**
 * ProductCellViewHolder.
 */
public class TransactionCellViewHolder extends RecyclerView.ViewHolder {

    private final TextView mTransactionAmountInOwnCurrencyView;
    private final TextView mTransactionAmountInGBPView;

    public TransactionCellViewHolder(View itemView) {
        super(itemView);
        mTransactionAmountInOwnCurrencyView = (TextView) itemView.findViewById(R.id.transaction_value_view);
        mTransactionAmountInGBPView = (TextView) itemView.findViewById(R.id.transaction_value_gbp_view);
    }

    public void bindTransaction(TransactionViewModel transaction) {
        mTransactionAmountInOwnCurrencyView.setText(transaction.mMonetaryValueInOwnCurrency);
        mTransactionAmountInGBPView.setText(transaction.mMonetaryValueInGBP);
    }
}
