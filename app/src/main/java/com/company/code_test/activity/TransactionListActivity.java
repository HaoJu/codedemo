package com.company.code_test.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TextView;

import com.company.code_test.R;
import com.company.code_test.adapter.TransactionListAdapter;
import com.company.code_test.presenter.TransactionListPresenter;
import com.company.code_test.view.model.TransactionViewModel;

import org.parceler.Parcels;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * TransactionListActivity.
 */
public class TransactionListActivity extends AppCompatActivity {
    private static final String TAG = "TransactionListActivity";
    private static final String KEY_PRODUCT_ID = "product_id";
    private static final String KEY_TRANSACTIONS = "transactions";

    private TransactionListPresenter mPresenter;
    private TransactionListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_list);
        initActionBar();
        mPresenter = new TransactionListPresenter();
        mPresenter.setTransactions((ArrayList<TransactionViewModel>) Parcels.unwrap(getIntent().getParcelableExtra(KEY_TRANSACTIONS)));
        initTotalValueBar(mPresenter.sumUpTransactionValueInGBP());
        initRecyclerView();
    }

    private String formatTotalTransactionValue(BigDecimal totalTransactionBigDecimal) {
        return getString(R.string.total_transactions_gbp) + totalTransactionBigDecimal.toEngineeringString();
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.transactions_for) + " " + getIntent().getStringExtra(KEY_PRODUCT_ID));
    }

    private void initTotalValueBar(BigDecimal totalTransactionValue) {
        TextView totalValueTextView = (TextView) findViewById(R.id.total_transaction_value_view);
        totalValueTextView.setText(formatTotalTransactionValue(totalTransactionValue));
    }

    private void initRecyclerView() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new TransactionListAdapter(mPresenter.getTransactions());
        recyclerView.setAdapter(mAdapter);
    }

    public static Intent newIntent(Context context, String productID, ArrayList<TransactionViewModel> transactions) {
        Intent intent = new Intent(context, TransactionListActivity.class);
        intent.putExtra(KEY_PRODUCT_ID, productID);
        intent.putExtra(KEY_TRANSACTIONS, Parcels.wrap(transactions));
        return intent;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
