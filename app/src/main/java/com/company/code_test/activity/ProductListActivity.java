package com.company.code_test.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.company.code_test.CompanyApplication;
import com.company.code_test.R;
import com.company.code_test.adapter.ProductListAdapter;
import com.company.code_test.presenter.ProductListPresenter;
import com.company.code_test.view.ProductListView;
import com.company.code_test.view.model.TransactionViewModel;

import java.util.ArrayList;

/**
 * ProductListActivity.
 */
public class ProductListActivity extends AppCompatActivity implements ProductListView {

    private static final String TAG = "ProductListActivity";
    private ProductListPresenter mPresenter;
    private ProductListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPresenter = new ProductListPresenter(this, CompanyApplication.getInstance().getDomainDataReader());
        initActionBar();
        initRecyclerView();
        mPresenter.fetchExchangeRateList();
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        getSupportActionBar().setTitle(getString(R.string.product_title));
    }

    private void initRecyclerView() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        ProductListAdapter.CellClickListener cellClickListener = new ProductListAdapter.CellClickListener() {
            @Override
            public void onCellClicked(String productID, ArrayList<TransactionViewModel> transactions) {
                navigateToTransactionListActivity(productID, transactions);
            }
        };
        mAdapter = new ProductListAdapter(mPresenter.getProductList(), cellClickListener);
        recyclerView.setAdapter(mAdapter);
    }

    private void navigateToTransactionListActivity(String productID, ArrayList<TransactionViewModel> transactions) {
        startActivity(TransactionListActivity.newIntent(this, productID, transactions));
    }

    @Override
    public void notifyUIForDataSetChange() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void reportError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
