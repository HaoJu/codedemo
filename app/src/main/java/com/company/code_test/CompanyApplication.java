package com.company.code_test;

import android.app.Application;
import android.support.annotation.NonNull;

import com.company.code_test.utility.DomainDataReader;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Application.
 */
public class CompanyApplication extends Application {

    private static CompanyApplication sApplication;
    private DomainDataReader mDomainDataReader;
    private Gson mGson;

    @Override
    public void onCreate() {
        super.onCreate();
        sApplication = this;
        mGson = initGson();
        mDomainDataReader = new DomainDataReader(this, mGson);
    }

    @NonNull
    private Gson initGson() {
        return new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
    }

    public static CompanyApplication getInstance() {
        return sApplication;
    }

    public DomainDataReader getDomainDataReader() {
        return mDomainDataReader;
    }
}
