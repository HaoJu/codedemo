package com.company.code_test.presenter;

import com.company.code_test.model.Rate;
import com.company.code_test.model.Transaction;
import com.company.code_test.utility.DomainDataReader;
import com.company.code_test.utility.TransactionValueExchangeConverter;
import com.company.code_test.view.ProductListView;
import com.company.code_test.view.model.TransactionViewModel;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

/**
 * ProductListPresenter.
 */
public class ProductListPresenter {

    private static final String TAG = "ProductListPresenter";
    private final String mFileTransactions = "responses/GET/first_data_set/transactions.json";
    private final String mFileExchangeRates = "responses/GET/first_data_set/rates.json";
    private final ProductListView mView;
    private final DomainDataReader mDomainDataReader;
    private final Func1<List<Transaction>, List<Transaction>> mToPassTransactionList = new Func1<List<Transaction>, List<Transaction>>() {
        @Override
        public List<Transaction> call(List<Transaction> transactions) {
            return transactions;
        }
    };
    private final Func1<List<TransactionViewModel>, Boolean> mToGroupTransactions
            = new Func1<List<TransactionViewModel>, Boolean>() {
        @Override
        public Boolean call(List<TransactionViewModel> transactions) {
            return groupTransactionsByProductID(transactions, mProductLists);
        }
    };
    private final Func1<Transaction, TransactionViewModel> mToConvertViewModels = new Func1<Transaction, TransactionViewModel>() {
        @Override
        public TransactionViewModel call(Transaction transaction) {
            return convertToViewModels(transaction);
        }
    };
    private final Func2<TransactionViewModel, TransactionViewModel, Integer> mToSortList =
            new Func2<TransactionViewModel, TransactionViewModel, Integer>() {
                @Override
                public Integer call(TransactionViewModel transactionViewModel, TransactionViewModel transactionViewModel2) {
                    return transactionViewModel.mProductID.compareToIgnoreCase(transactionViewModel2.mProductID);
                }
            };
    private final Func1<List<TransactionViewModel>, List<TransactionViewModel>> mToApplyExchange = new Func1<List<TransactionViewModel>, List<TransactionViewModel>>() {
        @Override
        public List<TransactionViewModel> call(List<TransactionViewModel> transactions) {
            return applyExchange(transactions, mExchangeConverter, mExchangeRates);
        }
    };
    private final Action1<Boolean> mToUpdateUI = new Action1<Boolean>() {
        @Override
        public void call(Boolean b) {
            mView.notifyUIForDataSetChange();
        }
    };
    private final Action1<List<Rate>> mToSaveExchangeRates = new Action1<List<Rate>>() {
        @Override
        public void call(List<Rate> rates) {
            mExchangeRates = rates;
            mExchangeConverter = new TransactionValueExchangeConverter();
        }
    };
    private final Action1<Throwable> mToHandleError = new Action1<Throwable>() {
        @Override
        public void call(Throwable throwable) {
            mView.reportError(throwable.getMessage());
        }
    };
    private final Action0 mToFetchProductList = new Action0() {
        @Override
        public void call() {
            fetchProductList();
        }
    };
    private TransactionValueExchangeConverter mExchangeConverter;
    private List<Rate> mExchangeRates;
    private ArrayList<ArrayList<TransactionViewModel>> mProductLists;

    public ProductListPresenter(ProductListView view, DomainDataReader reader) {
        mView = view;
        mDomainDataReader = reader;
        mExchangeConverter = new TransactionValueExchangeConverter();
        mExchangeRates = new ArrayList<>();
        mProductLists = new ArrayList<>();
    }

    private void fetchProductList() {
        Observable.just(mDomainDataReader.readTransactions(mFileTransactions)) // read transactions from IO.
                .flatMapIterable(mToPassTransactionList) // return same list to mark it iterable.
                .map(mToConvertViewModels) // convert domain model to view model.
                .toSortedList(mToSortList) // sort the list by product id, alphabetically.
                .map(mToApplyExchange) // convert transaction value in own currency to GBP value.
                .map(mToGroupTransactions) // group transactions, by using map (key, value).
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mToUpdateUI, mToHandleError); // handle or throw error.
    }

    /**
     * Fetch exchange rates first, then fetch product / transaction list.
     */
    public void fetchExchangeRateList() {
        Observable.just(mDomainDataReader.readExchangeRates(mFileExchangeRates))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mToSaveExchangeRates, mToHandleError, mToFetchProductList);
    }

    public ArrayList<ArrayList<TransactionViewModel>> getProductList() {
        return mProductLists;
    }

    /**
     * Convert Domain Transaction to View Model.
     * @param transaction
     * @return
     */
    public TransactionViewModel convertToViewModels(Transaction transaction) {
        TransactionViewModel viewModel = new TransactionViewModel();
        if (transaction != null) {
            if (transaction.amount != null) {
                viewModel.mTransactionAmount = transaction.amount;
            }
            if (transaction.sku != null) {
                viewModel.mProductID = transaction.sku;
            }
            if (transaction.currency != null) {
                viewModel.mTransactionCurrency = transaction.currency;
            }
        }
        return viewModel;
    }

    /**
     * Group transactions by transaction product id.
     *
     * @param transactions
     * @param productLists
     * @return
     */
    public Boolean groupTransactionsByProductID(List<TransactionViewModel> transactions, ArrayList<ArrayList<TransactionViewModel>> productLists) {
        LinkedHashMap productTransactionMap = new LinkedHashMap<>();
        for (TransactionViewModel transaction : transactions) {
            productTransactionMap.put(transaction.mProductID, new ArrayList<>());
        }
        for (TransactionViewModel transaction : transactions) {
            ((ArrayList) productTransactionMap.get(transaction.mProductID)).add(transaction);
        }
        return productLists.addAll(productTransactionMap.values());
    }

    /**
     * Apply exchange, convert non GBP value to GBP value.
     *
     * @param transactions
     * @param exchangeConverter
     * @return transactions with GBP value fields
     */
    public List<TransactionViewModel> applyExchange(List<TransactionViewModel> transactions, TransactionValueExchangeConverter exchangeConverter, List<Rate> exchangeRates) {
        for (TransactionViewModel transaction : transactions) {
            TransactionValueExchangeConverter.MonetaryValueHolder monetaryValue = exchangeConverter.applyExchange(transaction, exchangeRates);
            transaction.mMonetaryValueInGBP = monetaryValue.mMonetaryValueInGBP;
            transaction.mMonetaryValueInOwnCurrency = monetaryValue.mMonetaryValueInOwnCurrency;
            transaction.mTransactionAmountInGBP = monetaryValue.mValueInGBP;
        }
        return transactions;
    }
}
