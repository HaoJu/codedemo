package com.company.code_test.presenter;

import com.company.code_test.view.model.TransactionViewModel;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * TransactionListPresenter.
 */
public class TransactionListPresenter {

    private ArrayList<TransactionViewModel> mTransactions;

    public BigDecimal sumUpTransactionValueInGBP() {
        BigDecimal totalValueInGBP = new BigDecimal(0);
        for (TransactionViewModel transaction : mTransactions) {
            totalValueInGBP = totalValueInGBP.add(transaction.mTransactionAmountInGBP);
        }
        return totalValueInGBP;
    }

    public void setTransactions(ArrayList<TransactionViewModel> transactions) {
        mTransactions = transactions;
    }

    public ArrayList<TransactionViewModel> getTransactions() {
        return mTransactions;
    }
}
