# README #

A simple Android project demonstrate my ability to create structured, patterned, clean code using latest technologies / libs / methodologies,

* MVP for design and development pattern
* rxJava for functional programming
* Retrofit for network connection
* mockito for unit test
* SQLite and HTTP Cache

### What is this repository for? ###
* Demoing
* Pretty much anything you want.

### How do I get set up? ###
* Simply download the code and compile the projects.

### Who do I talk to? ###
* https://www.linkedin.com/in/hao-ju-b3663433